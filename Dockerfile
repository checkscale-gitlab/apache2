#FROM ubuntu:jammy
FROM ubuntu:jammy

#https://hub.docker.com/_/ubuntu/

# datos del Usuario
ARG USUARIO=huezohuezo1990
ARG UID=1990
ARG GID=1990
# Clave para el Usuario
ARG CLAVE=huezohuezo1990
#Creacion Usuario
RUN useradd -m ${USUARIO} --uid=${UID} && echo "${USUARIO}:${CLAVE}" | chpasswd
#Cambiar de /bin/sh a /bin/bash 
RUN usermod -s /bin/bash ${USUARIO}
# crear $HOME
#USER ${UID}:${GID}
WORKDIR /home/${USUARIO}


#ENV DEBIAN_FRONTEND noninteractive
ENV DEBIAN_FRONTEND noninteractive
ENV TZ=America/El_Salvador

# File Author / Maintainer
MAINTAINER huezohuezo1990


# Set the timezone.
ENV TZ=America/El_Salvador
RUN apt-get update 
RUN apt-get install -y tzdata 
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime 
RUN echo $TZ > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata


# Update the repository sources list
RUN apt-get update

#Usuario de Ubuntu como sudo
RUN apt install sudo -y
RUN usermod -aG sudo ${USUARIO}


# Install and run apache
RUN apt-get install -y apache2 && apt-get clean

# Install nano
RUN apt-get install -y nano && apt-get clean

# Install  net-tools + curl + wget 
RUN apt-get install -y net-tools curl wget && apt-get clean

# ping

RUN apt install -y iputils-ping && apt-get clean

# nmap

RUN apt install -y nmap && apt-get clean

# nmap

RUN apt-get install vim -y


# Reenviar registros de solicitudes y errores 
# al recolector de registros de Docker

RUN  ln -sf /dev/stdout /var/log/apache2/access.log 
RUN  ln -sf /dev/stderr /var/log/apache2/error.log 


# Permisos
RUN chmod -R u=rwx,g=rwx,o=rwx /var/www



#ENTRYPOINT ["/usr/sbin/apache2", "-k", "start"]


#ENV APACHE_RUN_USER www-data
#ENV APACHE_RUN_GROUP www-data
#ENV APACHE_LOG_DIR /var/log/apache2

EXPOSE 80
EXPOSE 443

CMD apachectl -D FOREGROUND


